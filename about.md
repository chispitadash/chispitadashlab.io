---
layout: page
title: Ahora
permalink: /ahora/
---

## ¿Qué estoy haciendo ahora?

- [Aprendiendo a programar en Python](https://www.udemy.com/automate/)

- [Aprendiendo Photoshop](http://www.youtube.com/watch?v=sF_jSrBhdlg)

- [Artes Marciales](https://www.youtube.com/watch?v=6QL0v5wLxKM)

- Meditación

- Vegetariana a tiempo parcial


### Leyendo:
- [The Asperkid's (Secret) Book of Social Rules](https://www.goodreads.com/book/show/14903347-the-asperkid-s-secret-book-of-social-rules) by Jennifer Cook O'Toole


### Viendo Series TV:
- [Dear white People](https://www.rottentomatoes.com/tv/dear_white_people)


### Pelis que quiero ver:
- [Free Guy](https://www.imdb.com/title/tt6264654/)

- [Under the Skin](https://www.imdb.com/title/tt1441395/)

*Última actualización:* Julio 14, 2020

*Inspiration:* I first learned about "now" pages from [Tania Rascia](https://taniarascia.com) who got the idea from [Derek Sivers](https://sivers.org/).
